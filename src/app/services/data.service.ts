import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public afDb: AngularFirestore, public afAuth: AngularFireAuth) { }

  getCollection(collection: string): Observable<any[]> {
    return this.afDb.collection(collection).valueChanges();
  }

  getDocument(collection: string, id: string): Observable<any> {
    return this.afDb.doc(`${collection}/${id}`).valueChanges();
  }

  updateDocument(collection: string, id: string, data: any) {
    this.afDb.doc(`${collection}/${id}`).update(data);
  }

  deleteDocument(collection: string, id: string) {
    this.afDb.doc(`${collection}/${id}`).delete();
  }

  createDocument(collection: string, data: any){
    return this.afDb.collection(collection).add(data);
  }

  register(user: User){
    const data = this.afAuth.createUserWithEmailAndPassword(user.email, user.password);
    const password = user.password;
    if(data){
      delete user.password;
      return this.createDocument('users', JSON.parse(JSON.stringify(user)))
        .then(ref => {
          if (user.role.indexOf('photographer')) {
            this.createDocument('photographer', JSON.parse(JSON.stringify({ userId: ref.id }))).then(ref => {});
          }
        })
        .then(() => {
          this.login(user.email, password);
        });
    }
  }

  login(email: string, password: string){
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  logout(){
    this.afAuth.signOut();  
  }
}

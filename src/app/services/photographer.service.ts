import { Injectable } from '@angular/core';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class PhotographerService {
  photographers: any[];

  constructor(public data: DataService) { 
    this.getPhotographers();
  }

  getPhotographers() {
    this.data.afDb
      .collection('users', ref => ref.where('role', 'array-contains', 'photographer'))
      .valueChanges()
      .subscribe(users => {
        this.photographers = users;
      });
  }

  updatePhotographer(id, data) {
    this.data.afDb
      .collection('photographers')
      .doc(id)
      .update(data)
      .then(() => {
        this.getPhotographers();
      });
  }
}

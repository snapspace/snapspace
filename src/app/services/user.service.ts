import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: any;
  
  constructor(public data: DataService) { 
    this.getUser();
  }

  register(user: User){
    return this
      .data
      .register(user)
      .then(() => { 
        this.login(user.email, user.password);
      });
  }

  login(email: string, password: string) {
    return this
      .data
      .login(email, password)
      .then(() => { 
        this.getUser();
      });
  }

  logout() {
    this.data.logout();
    this.user = null;
  }

  getUser() {
    this.data.afAuth.user.subscribe(authUser => {
      if (authUser) {
        this.data.afDb
          .collection('users', ref => ref.where('email', '==', authUser.email))
          .valueChanges()
          .subscribe(users => {
            if (users) {
              this.user = users[0];
            }
          });
      }
    });
  }
}

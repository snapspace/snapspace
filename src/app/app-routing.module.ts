import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { SearchComponent } from './components/search/search.component';

import { PhotographerDashboardComponent } from './components/Photographer/photographer-dashboard/photographer-dashboard.component';
import { PhotographerCalendarComponent } from './components/Photographer/photographer-calendar/photographer-calendar.component';
import { PhotographerInboxComponent } from './components/Photographer/photographer-inbox/photographer-inbox.component';
import { PhotographerSessionsComponent } from './components/Photographer/photographer-sessions/photographer-sessions.component';
import { PhotographerAlbumsComponent } from './components/Photographer/photographer-albums/photographer-albums.component';
import { PhotographerProfileComponent } from './components/Photographer/photographer-profile/photographer-profile.component';

import { UserDashboardComponent } from './components/User/user-dashboard/user-dashboard.component';
import { UserSessionsComponent } from './components/User/user-sessions/user-sessions.component';
import { UserInboxComponent } from './components/User/user-inbox/user-inbox.component';

import { AuthGuard, LoginGuard } from './auth-guard';

const routes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'register', component: SignupComponent, canActivate: [LoginGuard] },
  { path: 'search', component: SearchComponent },
  { 
    path: 'photographer', 
    data: { userType: 'photographer' },
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {path: '', component: PhotographerDashboardComponent },
      {path: 'calendar', component: PhotographerCalendarComponent },
      {path: 'inbox', component: PhotographerInboxComponent },
      {path: 'sessions', component: PhotographerSessionsComponent },
      {path: 'albums', component: PhotographerAlbumsComponent },
      {path: ':id', component: PhotographerProfileComponent }
    ]
  },
  { 
    path: 'user', 
    data: { userType: 'user' },
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {path: '', component: UserDashboardComponent },
      {path: 'inbox', component: UserInboxComponent },
      {path: 'sessions', component: UserSessionsComponent }
    ]
  }
  // { path: 'hero/:id',      component: HeroDetailComponent },
  // {
  //   path: 'heroes',
  //   component: HeroListComponent,
  //   data: { title: 'Heroes List' }
  // },
  // { path: '',
  //   redirectTo: '/heroes',
  //   pathMatch: 'full'
  // },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

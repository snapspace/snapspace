import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserDashboardComponent } from '../../components/User/user-dashboard/user-dashboard.component';
import { UserSessionsComponent } from '../../components/User/user-sessions/user-sessions.component';
import { UserInboxComponent } from '../../components/User/user-inbox/user-inbox.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    UserDashboardComponent,
    UserSessionsComponent,
    UserInboxComponent,
  ],
  exports: [
    UserDashboardComponent,
    UserSessionsComponent,
    UserInboxComponent,  
  ]
})
export class UserModule { }

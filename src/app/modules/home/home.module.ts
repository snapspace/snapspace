import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../../app-routing.module';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from '../../components/home/home.component';
import { LoginComponent } from '../../components/login/login.component';
import { SignupComponent } from '../../components/signup/signup.component';
import { SearchComponent } from '../../components/search/search.component';


@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule
  ],
  declarations: [
    HomeComponent,
    LoginComponent,
    SignupComponent,
    SearchComponent,
  ],
  exports: [
    HomeComponent,
    LoginComponent,
    SignupComponent,
    SearchComponent,  
  ]
})
export class HomeModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotographerProfileComponent } from '../../components/Photographer/photographer-profile/photographer-profile.component';
import { PhotographerInboxComponent } from '../../components/Photographer/photographer-inbox/photographer-inbox.component';
import { PhotographerSessionsComponent } from '../../components/Photographer/photographer-sessions/photographer-sessions.component';
import { PhotographerAlbumsComponent } from '../../components/Photographer/photographer-albums/photographer-albums.component';
import { PhotographerCalendarComponent } from '../../components/Photographer/photographer-calendar/photographer-calendar.component';
import { PhotographerDashboardComponent } from '../../components/Photographer/photographer-dashboard/photographer-dashboard.component';
import { PhotographerDashboardProfileComponent } from '../../components/Photographer/photographer-dashboard/photographer-dashboard-profile/photographer-dashboard-profile.component';
import { PhotographerDashboardSessionSettingsComponent } from '../../components/Photographer/photographer-dashboard/photographer-dashboard-session-settings/photographer-dashboard-session-settings.component';
import { PhotographerDashboardStatisticsComponent } from '../../components/Photographer/photographer-dashboard/photographer-dashboard-statistics/photographer-dashboard-statistics.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PhotographerProfileComponent,
    PhotographerInboxComponent,
    PhotographerSessionsComponent,
    PhotographerAlbumsComponent,
    PhotographerCalendarComponent,
    PhotographerDashboardComponent,
    PhotographerDashboardProfileComponent,
    PhotographerDashboardSessionSettingsComponent,
    PhotographerDashboardStatisticsComponent,
  ],
  exports: [
    PhotographerProfileComponent,
    PhotographerInboxComponent,
    PhotographerSessionsComponent,
    PhotographerAlbumsComponent,
    PhotographerCalendarComponent,
    PhotographerDashboardComponent,
    PhotographerDashboardProfileComponent,
    PhotographerDashboardSessionSettingsComponent,
    PhotographerDashboardStatisticsComponent,  
  ]
})
export class PhotographerModule { }

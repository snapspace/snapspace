import { Component, OnInit } from '@angular/core';
import { PhotographerService } from '../../services/photographer.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(public photographerService: PhotographerService) { }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photographer-profile',
  templateUrl: './photographer-profile.component.html',
  styleUrls: ['./photographer-profile.component.scss']
})
export class PhotographerProfileComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

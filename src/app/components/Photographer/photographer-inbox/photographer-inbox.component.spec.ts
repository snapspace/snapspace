import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerInboxComponent } from './photographer-inbox.component';

describe('PhotographerInboxComponent', () => {
  let component: PhotographerInboxComponent;
  let fixture: ComponentFixture<PhotographerInboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerInboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerInboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

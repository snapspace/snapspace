import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerDashboardSessionSettingsComponent } from './photographer-dashboard-session-settings.component';

describe('PhotographerDashboardSessionSettingsComponent', () => {
  let component: PhotographerDashboardSessionSettingsComponent;
  let fixture: ComponentFixture<PhotographerDashboardSessionSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerDashboardSessionSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerDashboardSessionSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

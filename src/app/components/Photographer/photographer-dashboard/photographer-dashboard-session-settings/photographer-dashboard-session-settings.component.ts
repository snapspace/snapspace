import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photographer-dashboard-session-settings',
  templateUrl: './photographer-dashboard-session-settings.component.html',
  styleUrls: ['./photographer-dashboard-session-settings.component.scss']
})
export class PhotographerDashboardSessionSettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

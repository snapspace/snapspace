import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerDashboardStatisticsComponent } from './photographer-dashboard-statistics.component';

describe('PhotographerDashboardStatisticsComponent', () => {
  let component: PhotographerDashboardStatisticsComponent;
  let fixture: ComponentFixture<PhotographerDashboardStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerDashboardStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerDashboardStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

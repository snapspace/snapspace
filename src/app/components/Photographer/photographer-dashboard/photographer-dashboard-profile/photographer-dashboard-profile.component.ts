import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photographer-dashboard-profile',
  templateUrl: './photographer-dashboard-profile.component.html',
  styleUrls: ['./photographer-dashboard-profile.component.scss']
})
export class PhotographerDashboardProfileComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

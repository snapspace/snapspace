import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerDashboardProfileComponent } from './photographer-dashboard-profile.component';

describe('PhotographerDashboardProfileComponent', () => {
  let component: PhotographerDashboardProfileComponent;
  let fixture: ComponentFixture<PhotographerDashboardProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerDashboardProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerDashboardProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

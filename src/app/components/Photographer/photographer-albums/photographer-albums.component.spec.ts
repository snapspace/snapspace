import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerAlbumsComponent } from './photographer-albums.component';

describe('PhotographerAlbumsComponent', () => {
  let component: PhotographerAlbumsComponent;
  let fixture: ComponentFixture<PhotographerAlbumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerAlbumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerAlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

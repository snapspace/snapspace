import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerSessionsComponent } from './photographer-sessions.component';

describe('PhotographerSessionsComponent', () => {
  let component: PhotographerSessionsComponent;
  let fixture: ComponentFixture<PhotographerSessionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerSessionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerSessionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

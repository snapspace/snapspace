import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerCalendarComponent } from './photographer-calendar.component';

describe('PhotographerCalendarComponent', () => {
  let component: PhotographerCalendarComponent;
  let fixture: ComponentFixture<PhotographerCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

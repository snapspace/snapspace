import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(public userService: UserService, public router: Router) { }

  ngOnInit() {
  }

  login() {
    this.userService
      .login(this.email, this.password)
      .then(() => { 
        this.router.navigate(['/home']);
      });
  }

}

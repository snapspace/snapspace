import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user: User;
  isPhotographer: boolean = false;
  password_confirm: '';

  constructor(public userService: UserService, public router: Router) { 
    this.user = new User();
  }

  ngOnInit() {
  }

  register(){
    this.user.role = this.isPhotographer ? ['user', 'photographer'] : ['user'];
    if(this.password_confirm === this.user.password ){
      this.userService
        .register(this.user)
        .then(() => { 
          this.router.navigate(['/home']);
        });;
    }
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

import { HomeModule } from './modules/home/home.module';
import { PhotographerModule } from './modules/photographer/photographer.module';
import { UserModule } from './modules/user/user.module';

import { DataService } from './services/data.service';
import { MessagingService } from './services/messaging.service';
import { AlbumService } from './services/album.service';
import { PhotographerService } from './services/photographer.service';
import { UserService } from './services/user.service';

import { AuthGuard, LoginGuard } from './auth-guard';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    
    HomeModule,
    PhotographerModule,
    UserModule,
    
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  providers: [
    DataService, 
    MessagingService,
    AlbumService,
    PhotographerService,
    UserService,
    AuthGuard,
    LoginGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

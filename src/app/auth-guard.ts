import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild,  ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { UserService } from './services/user.service'
import { Observable, of } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild  {
  constructor(private userService: UserService, private router: Router) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (!this.userService.user) {
      this.router.navigate(['/login']);
      return of(false);
    }
    return of(true);
  }
  
  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    return this.canActivate(next, state);  
  }
}

@Injectable()
export class LoginGuard implements CanActivate  {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if (this.userService.user) {
      this.router.navigate(['/home']);
      return of(false);
    }
    return of(true);
  }
}